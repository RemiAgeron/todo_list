<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="#">
  <link href="style.css" rel="stylesheet">
  <title>TODO LIST</title>
</head>
<body>
  <?php require('connexion.php'); ?>

  <H1>TODO LIST</H1>

  <?php if (empty($_POST)) {
    require('table.php');
  } else {
    isset($_POST['table']) ? require('form.php') : require('table.php');
  } ?>

</body>
</html>