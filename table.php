<?php $sqlTables = 'SHOW TABLES';
  $resTables = $mysqli->query($sqlTables); ?>

<?php foreach($resTables as $table) {

  $table = $table['Tables_in_todo_list'];

  $sqlColumns = "SHOW COLUMNS FROM $table";
  $resColumns = $mysqli->query($sqlColumns);
  $sqlData = "SELECT * FROM $table";
  $resData = $mysqli->query($sqlData); ?>

  <h3><?php echo $table ?></h3>

  <table>
    <tr>
      <?php foreach($resColumns as $column) { ?>
        <th>
          <?php echo $column['Field'] ?>
        </th>
      <?php } ?>
      <th>
        <form method="post">
          <input type="hidden" name="table" value="<?php echo $table ?>">
          <input type="hidden" name="task" value="insert">
          <input type="submit" value="Ajouter">
        </form>
      </th>
    </tr>
    <div>
      <?php foreach($resData as $data) { ?>
        <tr>
          <?php foreach($resColumns as $column) { ?>
            <td>
              <?php echo $data[$column['Field']] ?>
            </td>
          <?php } ?>
          <td>
            <form method="post">
              <input type="hidden" name="table" value="<?php echo $table ?>">
              <input type="hidden" name="task" value="update">
              <?php foreach($resColumns as $column) { ?>
                <input type="hidden" name="<?php echo $column['Field'] ?>" value="<?php echo $data[$column['Field']] ?>">
              <?php } ?>
              <input type="submit" value="Modifier">
            </form>
            <form method="post">
            <input type="hidden" name="table" value="<?php echo $table ?>">
              <input type="hidden" name="task" value="delete">
              <?php foreach($resColumns as $column) { ?>
                <input type="hidden" name="<?php echo $column['Field'] ?>" value="<?php echo $data[$column['Field']] ?>">
              <?php } ?>
              <input type="submit" value="Supprimer">
            </form>
          </td>
        </tr>
      <?php } ?>
    </div>
  </table>
<?php } ?>