<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$mysqli = new mysqli('localhost', 'remi', 'root', 'todo_list');

if ($mysqli->connect_error) {
  die("Connection failed: " . $mysqli->connect_error);
}
?>