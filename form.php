<?php function del($idA, $idB) { ?>
  <form action="delete.php" method="POST">
    <label><?php echo "Êtes-vous sûr de vouloir supprimer : $idA $idB"; ?></label>
    <?php if($idB != null) { ?>
      <input type="hidden" name="user_id" value="<?php echo $idA; ?>">
      <input type="hidden" name="story_id" value="<?php echo $idB; ?>">
    <?php } else { ?>
      <input type="hidden" name="id" value="<?php echo $idA; ?>">
    <?php } ?>
    <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>">
    <input type="submit" value="Valider">
  </form>
<?php } ?>

<h3><?php echo $_POST['table'] ?></h3>

<h4><?php echo $_POST['task'] ?></h4>

<?php switch ($_POST['table']) {
  case "UserStory":
    if($_POST['task'] == 'delete') { del($_POST['story_id'], null); }
    else { ?>
    <form action="<?php echo $_POST['task'] . ".php" ?>" method="POST">
      <textarea rows="1" name="story" placeholder="Story"><?php if(isset($_POST['story'])) echo $_POST['story']; ?></textarea>
      <input type="hidden" name="id" value="<?php if(isset($_POST['story_id'])) echo $_POST['story_id']; ?>">
      <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>">
      <input type="submit" value="Valider">
    </form>
    <?php }
    break;
  case "Users":
    if($_POST['task'] == 'delete') { del($_POST['user_id'], null); }
    else { ?>
    <form action="<?php echo $_POST['task'] . ".php" ?>" method="POST">
      <input type="email" name="email" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
      <input type="text" name="firstname" placeholder="Prénom" value="<?php if(isset($_POST['firstname'])) echo $_POST['firstname']; ?>">
      <input type="text" name="lastname" placeholder="Nom" value="<?php if(isset($_POST['lastname'])) echo $_POST['lastname']; ?>">
      <input type="hidden" name="id" value="<?php if(isset($_POST['user_id'])) echo $_POST['user_id']; ?>">
      <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>">
      <input type="submit" value="Valider">
    </form>
    <?php }
    break;
  case "JoinTable":
    if($_POST['task'] == 'delete') { del($_POST['user_id'], $_POST['story_id']); }
    else { ?>
    <form action="<?php echo $_POST['task'] . ".php" ?>" method="POST">
      <?php $sqlU = "SELECT * FROM Users";
      $resU = $mysqli->query($sqlU); ?>
      <select name="user_id">
        <?php foreach($resU as $user) { ?>
          <option <?php if(isset($_POST['user_id'])) { if($_POST['user_id'] == $user['user_id']) echo "selected"; } ?> value="<?php echo $user['user_id'] ?>">
            <?php echo "$user[firstname] $user[lastname]" ?>
          </option>
        <?php } ?>
      </select>
      <?php $sqlUS = "SELECT * FROM UserStory";
      $resUS = $mysqli->query($sqlUS); ?>
      <select name="story_id">
        <?php foreach($resUS as $story) { ?>
          <option <?php if(isset($_POST['story_id'])) { if($_POST['story_id'] == $story['story_id']) echo "selected"; } ?> value="<?php echo $story['story_id'] ?>">
            <?php echo "$story[story]" ?>
          </option>
        <?php } ?>
      </select>
      <input type="hidden" name="id" value="<?php if(isset($_POST['user_id'])) echo $_POST['id']; ?>">
      <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>">
      <input type="submit" value="Valider">
    </form>
    <?php }
    break;
  default:
    header("Location: index.php");
} ?>

<a href="index.php"><button>Annuler</button></a>